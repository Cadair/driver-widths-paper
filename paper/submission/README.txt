Paper originally compiled with pdfTeX 3.14159265-2.6-1.40.17 (TeX Live 2016/Arch Linux).
Requires these packages:
- graphicx
- pgf
- float
- import
- cleveref
- subcaption
- microtype
- amsmath
- amssymb
- bm


Running the following commands should compile leonard_etal_2018.tex and produce leonard_etal_2018.pdf

pdflatex --shell-escape --interaction=batchmode leonard_etal_2018.tex
bibtex leonard_etal_2018
makeindex leonard_etal_2018.aux
makeindex leonard_etal_2018.idx
makeindex leonard_etal_2018.nlo -s nomencl.ist -o leonard_etal_2018.nls
pdflatex --shell-escape --interaction=batchmode leonard_etal_2018.tex
makeindex leonard_etal_2018.nlo -s nomencl.ist -o leonard_etal_2018.nls
pdflatex --synctex=1 --shell-escape --interaction=batchmode leonard_etal_2018.tex
