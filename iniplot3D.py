from mayavi import mlab
import yt
import pysac.yt
import matplotlib.pyplot as plt
import astropy.units as u
import sys
import os
import glob

import numpy as np
import yt.mods as ytm
from tvtk.util.ctf import PiecewiseFunction
from tvtk.util.ctf import ColorTransferFunction

from astropy.io import fits

#pysac imports
import pysac.io.yt_fields
import pysac.analysis.tube3D.tvtk_tube_functions as ttf
#import pysac.plot.tube3D.mayavi_plotting_functions as mpf
import pysac.plot.mayavi_plotting_functions as mpf

thisfile = yt.load('/home/drew/fastdata/driver-widths/gdf/Slog_p90-0_4-8_0-30/Slog_p90-0_4-8_0-30_00001.gdf')
#print thisfile.field_list

"""for field in thisfile.field_list:
    myplot = yt.SlicePlot(thisfile, 'x', field, axes_unit='m')
    myplot.set_cmap(field, 'viridis')
    myplot.save('/fastdata/sm1ajl/initestplots/')"""

"""beta = 'plasma_beta'

betaplot = yt.SlicePlot(thisfile, 'x', beta, axes_unit='Mm', origin='native')#, center=[0.0, 0.0, 0.0]*u.Mm)
#minvar, maxvar = betaplot.data_source[beta].min(), betaplot.data_source[beta].max()
betaplot.set_log(beta, False)
betaplot.set_zlim(beta, 0.1, 10)
betaplot.set_zlim(beta, 0.5, 1.5)
#betaplot.zoom(2)
#betaplot.annotate_contour('plasma_beta', take_log=False, ncont=1, label=True, clim=(1, 1))
betaplot.set_cmap(beta, 'coolwarm')
betaplot.save('/fastdata/sm1ajl/initestplots/')

print(thisfile.all_data().quantities.extrema(beta),
      betaplot.data_source[beta].mean())
print thisfile.domain_dimensions


betaplot = yt.SlicePlot(thisfile, 'z', beta, axes_unit='Mm', origin='native')#, center=[0.0, 0.0, -0.8]*u.Mm)
betaplot.set_log(beta, False)
#betaplot.set_zlim(beta, 0.1, 10)
betaplot.set_zlim(beta, 0.5, 1.5)
#betaplot.zoom(2)
#betaplot.annotate_contour('plasma_beta', take_log=False, ncont=1, label=True, clim=(1, 1))
betaplot.set_cmap(beta, 'coolwarm')
betaplot.save('/fastdata/sm1ajl/initestplots/')

print(thisfile.all_data().quantities.extrema(beta),
      betaplot.data_source[beta].mean())
print thisfile.domain_dimensions
"""

#cg = thisfile.h.grids[0]
cube_slice = np.s_[:,:,:-5]

for i in dir(thisfile): print(i)

mlab.options.offscreen = True
fig = mlab.figure()

#Create a bfield tvtk field, in mT
bfield = mlab.pipeline.vector_field(thisfile['mag_field_x'][cube_slice] * 1e3,
                                    thisfile['mag_field_y'][cube_slice] * 1e3,
                                    thisfile['mag_field_z'][cube_slice] * 1e3,
                                    name="Magnetic Field",figure=fig)

#Create a scalar field of the magntiude of the vector field
bmag = mlab.pipeline.extract_vector_norm(bfield, name="Field line Normals")

fig.scene.save('testplot')
