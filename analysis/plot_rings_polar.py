import os
import sys

import numpy as np
import matplotlib.pyplot as plt

sys.path.append("..")
from scripts.sacconfig import SACConfig

cfg = SACConfig()

for tube_r in cfg.tube_radii:
    fig, axes = plt.subplots(3, (12 // 3),
                            subplot_kw={'projection': 'polar'}, figsize=(20, 11))
    fig.suptitle(tube_r)
    for i, n in enumerate(range(5, 125, 10)):
        ax = axes.flat[i]
        dis = np.load(os.path.join(cfg.data_dir, tube_r, "{}_h{}_distance.npy".format(cfg.get_identifier(), n)))

        n_t, n_theta = dis.shape

        d1 = dis - dis[0][None]
        d1 *= 15.6e3

        sym_max = np.max((np.abs(np.min(d1)), np.max(d1)))
        vmin = sym_max * -1
        vmax = sym_max

        R, T = np.meshgrid(range(n_t), np.linspace(0, 2 * np.pi, n_theta))

        c = ax.pcolormesh(T, R, d1.T, vmin=vmin, vmax=vmax, cmap='RdBu_r')
        cbar = plt.colorbar(c, ax=ax, shrink=0.9, pad=0.1)
        cbar.ax.set_ylabel('Radial Displacement [m]')

        ax.set_title("Height {} km".format((n * 12.5)), y=1.15)
        ax.set_yticks([0, 100, 200])
        ax.set_rlabel_position(90)
        ax.set_theta_zero_location("N")
        ax.set_theta_direction(-1)
        ax.grid()
        label_position = ax.get_rlabel_position()
        ax.text(
            np.radians(label_position + 10),
            ax.get_rmax() / 2.,
            'Time [s]',
            ha='center',
            va='center')

        # tick locations
        thetaticks = np.arange(0, 360, 45)
        # set ticklabels location at 1.3 times the axes' radius
        ax.set_thetagrids(thetaticks, frac=1.2)

        ax.set_xlabel("Angle around flux\n surface [degrees]")

    plt.tight_layout()
plt.show()
