# coding: utf-8

# # Decomposing Velocity and Wave Energy Flux in 3D

# Stuart J. Mumford - The University of Sheffield - 2016

# This notebook is a modified version of the Flux Surface Analysis notebook to
# calculate the radial distance from the orginal field axis for all points on
# the flux surface to enable identification of kink / sausage modes.

# ## Imports and Setup:

# As you may expect this code is quite complex and relies on many external and
# custom bits of code. Many of the wrapper functions to convert between `numpy`
# arrays and `tvtk` datasets and to perform various operations on `tvtk`
# objects have been added to the pysac helper library which you will need.

import sys

import numpy as np
import matplotlib.pyplot as plt
import yt
from mayavi import mlab
from tvtk.api import tvtk

import pysac.yt
import pysac.analysis.tube3D.tvtk_tube_functions as ttf
import pysac.plot.mayavi_plotting_functions as mpf

# Import this repos config
sys.path.append("..")
from scripts.sacconfig import SACConfig
cfg = SACConfig()

# In[ ]:


def mlab_view(scene,
              azimuth=153,
              elevation=62,
              distance=400,
              focalpoint=np.array([25., 63., 60.]),
              aa=16):
    scene.anti_aliasing_frames = aa
    mlab.view(
        azimuth=azimuth,
        elevation=elevation,
        distance=distance,
        focalpoint=focalpoint)

cfg.period = 240.0
cfg.amp = 'A10'

# Load the data series into `yt`


timeseries = yt.load(
    "/home/stuart/Git/Thesis/thesis/Chapter3/Data/Slog_p240-0_A10_B005_00001.gdf")


# Define which step
n = 300

# Slices
cube_slice = np.s_[:, :, :-5]
x_slice = np.s_[:, :, :, :-5]

tube_r = cfg.tube_radii[2]


# if running this creates a persistant window just get it out of the way!
#mlab.options.offscreen = True
fig = mlab.figure(bgcolor=(1, 1, 1))
scene = fig.scene


# Use the first timestep
ds = timeseries
cg = ds.index.grids[0]

# The first step is to create a `tvtk` dataset for the magentic field, `mayavi`
# provides the best and potentially only functional wrapper that allows you to
# do this for a 3D vector field.


# Create a bfield tvtk field, in mT
bfield = mlab.pipeline.vector_field(
    cg['mag_field_x'][cube_slice] * 1e3, cg['mag_field_y'][cube_slice] * 1e3,
    cg['mag_field_z'][cube_slice] * 1e3, name="Magnetic Field", figure=None)
# Create a scalar field of the magntiude of the vector field
bmag = mlab.pipeline.extract_vector_norm(bfield, name="Field line Normals")

# Define the size of the domain
xmax, ymax, zmax = np.array(cg['mag_field_x'][cube_slice].shape) - 1
domain = {'xmax': xmax, 'ymax': ymax, 'zmax': zmax}

# ## Creating A Flux Tube

# ### Fieldline seed points

# The initial conditions used in this work are axissymetric, so a flux tube can
# be constructed by defining a circle around that axis of symmetry. As the
# magnetic field is weaker at the top the seed points are placed there and
# traced downwards through the domian.

# In[ ]:

# Add axes:
axes, outline = mpf.add_axes(
    np.array(zip(ds.domain_left_edge, ds.domain_right_edge)).flatten() / 1e8)

# In[ ]:

surf_seeds_poly = ttf.make_circle_seeds(100, int(tube_r[1:]), **domain)
seeds = np.array(surf_seeds_poly.points)

# ### Creation of a Flux Surface

# To compute the surface we access the `tvtk` classes for streamline
# computation and employ the `RuledSurfaceFilter` to create a surface of
# polygons from the line. The snippet below is implemented in
# `pysac.analysis.tube3D.tvtk_tube_functions.create_flux_surface(bfield,
# surf_seeds)`, but is used verbatim below as an example.

# In[ ]:

# Make a streamline instance with the bfield
surf_field_lines = tvtk.StreamTracer()
# bfield is a mayavi data object, we require a tvtk dataset which can be access
# thus:
surf_field_lines.input = bfield.outputs[0]

surf_field_lines.source = surf_seeds_poly
surf_field_lines.integrator = tvtk.RungeKutta4()
surf_field_lines.maximum_propagation = 1000
surf_field_lines.integration_direction = 'backward'
surf_field_lines.update()

# Create surface from 'parallel' lines
surface = tvtk.RuledSurfaceFilter()
surface.input = surf_field_lines.output
surface.close_surface = True
surface.pass_lines = True
surface.offset = 0
surface.distance_factor = 30
surface.ruled_mode = 'point_walk'
surface.update()

# Set the lines to None to remove the input lines from the output
surface.output.lines = None

# To visualise this surface we add it to our running `mayavi` pipeline:

# We can turn on displaying the edges of the polygons that the surface is
# comprised of and zoom in to highlight the construction of the surface:


points = np.array(surface.output.points)
points -= [63, 63, 0]
points

distance = np.sqrt((points[:, 0]**2 + points[:, 1]**2))
distance

# To visualise the vectors in `mayavi` we need to add them to the surface
# `tvtk` object,

pd_dis = tvtk.PointData(scalars=distance)
pd_dis.scalars.name = "distance"

poly_out = surface.output
poly_out.point_data.add_array(pd_dis.scalars)

flux_surface2 = mlab.pipeline.surface(surface.output)

# Set the surface component to be the azimuthal component
flux_surface2.parent.parent.point_scalars_name = 'distance'

flux_surface2.module_manager.scalar_lut_manager.lut.table = plt.get_cmap(
    'Reds')(range(255)) * 255
lim = np.max([np.nanmax(surface.output.point_data.scalars),
              np.abs(np.nanmin(surface.output.point_data.scalars))])
flux_surface2.module_manager.scalar_lut_manager.data_range = np.array([0, lim])

# Add a colourbar:

surf_bar = mpf.add_colourbar(
    flux_surface2, [0.84, 0.2], [0.11, 0.31],
    title='',
    label_fstring='%#4.2f',
    number_labels=5,
    orientation=1,
    lut_manager='scalar')
mpf.add_cbar_label(surf_bar,
                   'Distance from original axis\n          [grid points]')

# Below is a render with the surface coloured according to the azimuthal
# velocity.

mlab.show()


