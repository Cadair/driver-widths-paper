import os
import sys

import numpy as np
from tvtk.api import tvtk
import vtk

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    mpi = True
    mpi = mpi and (size != 1)
except ImportError:
    mpi = False
    rank = 0

from pysac.analysis.tube3D import tvtk_tube_functions as ttf

# Import this repos config
sys.path.append("..")
from scripts.sacconfig import SACConfig
from scripts import plotting_helpers as ph

cfg = SACConfig()
cfg.print_config()

for tube_r in cfg.tube_radii:
    print("Starting Tube {}".format(tube_r))
    surface_files = np.array(ph.glob_files(cfg, tube_r, "Fieldline*"))
    rank_indices = np.array_split(np.arange(len(surface_files)), size)
    if not mpi:
        rank_indices = rank_indices[0]

    n_lines = 100  # Number of lines in theta

    if mpi:
        rank_indices = rank_indices[rank]

    for ring_height in range(5, 125, 10):  # in grid points
        print("Starting height {}".format(ring_height))

        theta = np.pi / 4

        theta_pos = np.zeros([len(surface_files), n_lines, 3])

        # Make variables for line intersection
        t = vtk.mutable(0)
        pcoords = [0.0, 0.0, 0.0]
        subId = vtk.mutable(0)

        for n in rank_indices:
            vtp = surface_files[n]
            print(rank, n, vtp)

            # Build CellLocator for this time step
            surface = ttf.read_step(vtp)
            loc = tvtk.CellLocator()
            loc.data_set = surface
            loc.build_locator()

            for k, theta in enumerate(np.linspace(0, 2 * np.pi, n_lines)):
                # Calculate end points of line
                p1 = [64, 64, ring_height]
                p2 = [64 + (64 * np.sin(theta)),
                      64 + (64 * np.cos(theta)), ring_height]

                pos = [0.0, 0.0, 0.0]

                # Intersect with line, output is in the pos variable
                loc.intersect_with_line(p1, p2, 0.00001, vtk.mutable(0), pos,
                                        pcoords, subId)
                theta_pos[n, k] = pos

        if mpi:
            theta_pos_r0 = comm.gather(theta_pos, root=0)
        else:
            theta_pos_r0 = theta_pos[None]

        if rank == 0:
            theta_pos = np.concatenate(theta_pos_r0)

            theta_pos2 = theta_pos - [64, 64, 0]
            dis = np.sqrt(theta_pos2[:, :, 0]**2 + theta_pos2[:, :, 1]**2)

            np.save(os.path.join(cfg.data_dir, tube_r, "{}_h{}_theta_pos.npy".format(cfg.get_identifier(), ring_height)),
                    theta_pos2)
            np.save(os.path.join(cfg.data_dir, tube_r, "{}_h{}_distance.npy".format(cfg.get_identifier(), ring_height)),
                    dis)
