# coding: utf-8
#import pysac.analysis.tube3D.process_utils as utils
import yt
import pysac.yt
import yt.units as u
from yt.visualization.api import get_multi_plot
import numpy as np
import matplotlib as mpl
import matplotlib.animation as ani
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import AxesGrid
import sys
import os
from yt.utilities.physical_constants import mu_0


def _theta(field, data):
    x, y = data['x'], data['y']
    return u.yt_array.YTArray(np.arctan2(y, x), 'radian')


def _v_theta(field, data):
    x, y, theta = data['velocity_x'], data['velocity_y'], data['theta']
    return u.yt_array.YTArray(-x*np.sin(theta) + y*np.cos(theta), x.units)


def _v_theta_abs(field, data):
    return u.yt_array.YTArray(abs(data['v_theta']), data['v_theta'].units)


def _v_r(field, data):
    x, y, theta = data['velocity_x'], data['velocity_y'], data['theta']
    return u.yt_array.YTArray(x*np.cos(theta) - y*np.sin(theta), x.units)


def _alfvenspeed(field, data):
    bx = data['mag_field_x_bg'] + data['mag_field_x_pert']
    by = data['mag_field_y_bg'] + data['mag_field_y_pert']
    bz = data['mag_field_z_bg'] + data['mag_field_z_pert']
    B = np.sqrt(bx**2 + by**2 + bz**2)

    return u.yt_array.YTArray(B / np.sqrt(data['density_bg'] + data['density_pert']), 'km/s')


def _thermal_pressure_pert(field, data):
    #p = (\gamma -1) ( e - \rho v^2/2 - B^2/2)
    g1 = data.ds.parameters.get('gamma', 5./3.) -1
    if data.ds.dimensionality == 2:
        kp = (data['density'] * (data['velocity_x']**2 +
                                 data['velocity_y']**2))/2.
    if data.ds.dimensionality == 3:
        kp = (data['density'] * (data['velocity_x']**2 +
              data['velocity_y']**2 + data['velocity_z']**2))/2.
    return g1 * (data['internal_energy_pert'] - kp - data['mag_pressure_pert'])


def _thermal_pressure_bg(field, data):
    return data['thermal_pressure'] - data['thermal_pressure_pert']


def _thermal_pressure_frac(field, data):
    return data['thermal_pressure_bg'] / data['thermal_pressure_pert']


def _mag_pressure_pert(field, data):
    if data.ds.dimensionality == 2:
        Bp_sq_2 = (data['mag_field_x_pert']**2 +
                   data['mag_field_y_pert']**2) / (2. * mu_0)
    if data.ds.dimensionality == 3:
        Bp_sq_2 = (data['mag_field_x_pert']**2 + data['mag_field_y_pert']**2 +
                   data['mag_field_z_pert']**2) / (2. * mu_0)
    return (((data['magnetic_field_strength_bg'] * data['magnetic_field_strength_pert']) / mu_0) 
            + Bp_sq_2)# / np.sqrt(mu_0)


yt.add_field('theta', function=_theta, units='radian')
yt.add_field('v_theta', function=_v_theta, units='km/s')
yt.add_field('v_theta_abs', function=_v_theta_abs, units='km/s')
yt.add_field('v_r', function=_v_r, units='km/s')
yt.add_field('alfvenspeed', function=_alfvenspeed, units='km/s')
yt.add_field('thermal_pressure_pert', function=_thermal_pressure_pert, units='Pa')
yt.add_field('thermal_pressure_bg', function=_thermal_pressure_bg, units='Pa')
yt.add_field('thermal_pressure_frac', function=_thermal_pressure_frac, units='dimensionless')
yt.add_field('mag_pressure_pert', function=_mag_pressure_pert, units='Pa')

plotvar = sys.argv[1]

for wid in ['0-15', '0-20', '0-25', '0-30', '0-35']:
  ts = yt.load('/fastdata/sm1ajl/driver-widths/gdf/Slog_p90-0_???_{0}/Slog_p90-0_???_{0}_00???.gdf'.format(wid))
  print len(ts)

  for t in range(0, len(ts), 10):#2):
  #for t in range(0, 210, 30):
    ds = ts[t]

    """
    Plot vertical slice at various heights
    """
    fig = plt.figure()#figsize=(15, 8))
    grid = AxesGrid(fig, (0.075,0.075,0.85,0.85), nrows_ncols=(2, 2),
                    cbar_location="right", cbar_mode="each", cbar_size="4%", cbar_pad="2%",
                    axes_pad=1.0)

    minv, maxv = ds.all_data().quantities.extrema("velocity_magnitude").to('m/s')

    minvar, maxvar = ds.all_data().quantities.extrema(plotvar)
    print t, "====", minv, maxv, '===='
    if 'velocity' in plotvar:
        minvar = minvar.to('m/s')
        maxvar = maxvar.to('m/s')
    #if plotvar == 'velocity_z':
    #    extreme = 0.5 * u.km / u.s
    #else:
    extreme = max(abs(minvar), abs(maxvar))
    extremev = max(abs(minv), abs(maxv))

    if 'mag' in plotvar and 'pressure' not in plotvar and 'velocity' not in plotvar:
        print '====', minvar.to_equivalent('gauss', 'CGS'), maxvar.to_equivalent('gauss', 'CGS'), '===='
    else:
        print '====', minvar, maxvar, '===='

    #for i, height in enumerate([0.1, 0.8]):
    for i, height in enumerate([0.1, 0.5, 1.0, 1.5]):
        slc = yt.SlicePlot(ds, 'z', plotvar, axes_unit='Mm', origin='native',
                      center=[1.0, 1.0, height]*u.Mm)

        minvar, maxvar = slc.data_source[plotvar].min(), slc.data_source[plotvar].max()
        if 'vel' in plotvar:
            print minvar.to('m/s'), maxvar.to('m/s'),

        extreme = max(abs(minvar), abs(maxvar))
        absvar = abs(slc.data_source[plotvar])
        var = slc.data_source[plotvar]
        if 'velocity' in plotvar or 'v_' in plotvar:
            slc.set_unit(plotvar, 'm/s')
            extreme = extreme.to('m/s')
        print extreme

        slc.set_cmap(plotvar, 'viridis')
        #slc.set_zlim(plotvar, 0, maxvar.value)
        slc.set_log(plotvar, False)

        slc.annotate_velocity(scale=2e5)
        slc.annotate_title('Height = {} Mm'.format(str(height)))
        slc.annotate_timestamp(text_args={'color': 'black'})
        halfmax = slc.data_source['magnetic_field_strength'].max()/2
        slc.annotate_contour('magnetic_field_strength', plot_args={color: 'black'},
                             take_log=False, ncont=1, clim=(halfmax, halfmax))

        plot = slc.plots[plotvar]
        plot.figure = fig
        plot.axes = grid[i].axes
        plot.cax = grid.cbar_axes[i]
        slc._setup_plots()

    fname = '/data/sm1ajl/driver-widths-paper/figs/{}/horizontal_slice/{}/{:03}'.format(wid, plotvar, t)
    if not os.path.exists(os.path.dirname(fname)):
        os.makedirs(os.path.dirname(fname))
    slc.save(fname)
    plt.close()

    fig = plt.figure(figsize=(18, 8))
    """grid = AxesGrid(fig, (0.075,0.075,0.85,0.85), nrows_ncols=(1, 2),
                    cbar_location="right", cbar_mode="each", cbar_size="5%", cbar_pad="0%",
                    axes_pad=1.0)"""

    #for i, depth in enumerate([0.01, 1.0]):
    for i, depth in enumerate([0.0]):
        slc = yt.SlicePlot(ds, 'x', plotvar, axes_unit='Mm', origin='native',
                      center=[depth, 1.0, 0.8]*u.Mm)

        minvar, mavar = slc.data_source[plotvar].min(), slc.data_source[plotvar].max()
        if 'vel' in plotvar:
            print minvar.to('m/s'), maxvar.to('m/s'),

        extreme = max(abs(minvar), abs(maxvar))
        absvar = abs(slc.data_source[plotvar])
        var = slc.data_source[plotvar]
        if 'velocity' in plotvar or 'v_' in plotvar:
            slc.set_unit(plotvar, 'm/s')
            extreme = extreme.to('m/s')
        print extreme

        slc.set_cmap(plotvar, 'viridis')
        #slc.set_zlim(plotvar, 0, maxvar.value)
        slc.set_log(plotvar, False)

        seed_points = np.zeros([11,2]) + 1.52
        #seed_points[:, 0] = np.linspace(-0.99, 0.95, seed_points.shape[0],
        seed_points[:, 0] = np..linspace(0.01, 1.95, seed_points.shape[0],
                                       endpoint=True)
        slc.annotate_streamlines('mag_field_y', 'mag_field_z',
                                 field_color='magnetic_field_strength',
                                 plot_args={'start_points': seed_points,
                                            'density': 15,
                                            'cmap': 'inferno_r', 'linewidth':2,
                                            })

        slc.annotate_title('x = {} Mm'.format(str(depth)))
        slc.annotate_timestamp(text_args={'color': 'white'})

        plot = slc.plots[plotvar]
        #plot.figure = fig
        #plot.axes = grid[i].axes
        #plot.cax = grid.cbar_axes[i]
        slc._setup_plots()

    fname = '/data/sm1ajl/driver-widths-paper/figs/{}/vertical_slice/{}/{:03}'.format(wid, plotvar, t)
    if not os.path.exists(os.path.dirname(fname)):
        os.makedirs(os.path.dirname(fname))
    slc.save(fname)
    plt.close()

print "\n\n=====\nPlots complete\n=====\n\n"
