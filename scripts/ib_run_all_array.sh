#!/bin/bash -e
#$ -l h_rt=15:00:00
#$ -cwd
#$ -l arch=intel*
#$ -l mem=8G
#$ -l rmem=4G
#$ -pe openmpi-ib 16
#$ -P mhd
#$ -q mhd.q
#$ -N driver_widths
#$ -j y
#$ -t 1-5

source $HOME/.bashrc
module purge
module load apps/python/conda
module load mpi/gcc/openmpi/1.10.0
source activate mpi-sac
echo $(which python)
echo $(which mpirun)
echo $(which mpif90)
#################################################################
################ Set the Parameters for the array ###############
#################################################################


#################################################################
####################### Run the Script ##########################
#################################################################

#### Setup and Configure ####
i=$((SGE_TASK_ID - 1))

BASE_DIR=/home/sm1ajl/driver-widths-paper
TMP_DIR=$(mktemp -d --tmpdir=/fastdata/sm1ajl/temp_run/)

cp -r $BASE_DIR $TMP_DIR
cd $TMP_DIR/driver-widths-paper
pwd

./configure.py set SAC --usr_script="Slog"
./configure.py set driver --exp_fac=0.15 --period=90

#### Run SAC ####
deltas=( "0.15" "0.20" "0.25" "0.30" "0.35" )
#amps=( "7.2274441701395986" "5.2788458200228927" "4.167609775756989" "3.4471974045012397" "2.9411894675997523" )
amps=( "10.221149567" "7.465415352" "5.893890268" "4.875073322" "4.159470035" )

echo ${deltas[i]} ${amps[i]} ${amps[i]:0:3}
./configure.py set SAC --runtime=300
./configure.py set driver --delta_x=${deltas[i]} --delta_y=${deltas[i]} --amp=${amps[i]:0:3} --fort_amp=${amps[i]}d0
./configure.py print;
./configure.py compile sac --clean;

python ./run.py SAC --mpi
python ./run.py gdf --mpi

#### Run the CODE! ####
tube_radii=( 'r60' 'r30' 'r10' )
for tuber in "${tube_radii[@]}"
do
    echo $tuber
    #xvfb-run --auto-servernum python ./run.py analysis --mpi --np=16 --tube-r=$tuber
    xvfb-run --auto-servernum python ./run.py analysis --mpi --np=1 --tube-r=$tuber
    set +e
    kill $XVFBPID
    set -e
done

###### I Done it now ########
rm -rf $TMP_DIR
