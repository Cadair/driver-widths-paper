import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from sacconfig import SACConfig

cfg = SACConfig()

radii = range(3, 66, 3)
#heights = range(15, 125, 30)
heights = [15, 65, 115]

alldists = np.zeros((len(radii)*2, len(heights), 270, 100))
#alldists = np.zeros((max(radii)+1, len(heights), 270, 100))
#alldists = np.zeros(((max(radii)+1), len(heights), 270, 100))
radcoords = np.zeros((len(radii)*2, len(heights)))

for j, r in enumerate(radii):
    tube_r = 'r{:02}'.format(r)
    for i, h in enumerate(heights):
        #print os.path.join(cfg.data_dir, tube_r, "{}_h{}_distance.npy".format(cfg.get_identifier(), h))
        dis = np.load(os.path.join(cfg.data_dir, tube_r, "{}_h{}_distance.npy".format(cfg.get_identifier(), h)))[:270]
        #sys.exit(0)
        n_t, n_theta = dis.shape
        #if i == 0:
        #    print(dis.mean(), int(dis.mean()))
        radcoords[j*2, i] = dis.mean()
        d1 = dis - dis[0][None]
        d1 *= 15.6e3

        alldists[j*2, i, :, :] = d1
        #alldists[r, i, :, :] = d1
        #alldists[int(dis.mean()), i, :, :] = d1
print('Files loaded')

l1 = 38
#sep = 0.8
#radcoords[1::2] = radcoords[::2] + sep
sep = 1.04
radcoords[1::2] = radcoords[::2] * sep
plotlims = [450, 320, 500, 650]
for i, h in enumerate(heights):
    print('Building slices at height {}'.format(h))
    for t, time in enumerate(range(0, 270, 5)):
        #fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
        fig = plt.figure(figsize=(21, 9))
        ax = fig.add_subplot(121, projection='polar')
        hslice = alldists[:, i, time, :]
        n_r, n_theta = hslice.shape
        sym_max = np.max((np.abs(np.min(hslice)), np.max(hslice)))
        vmin = sym_max * -1
        vmax = sym_max
    
        #R, T = np.meshgrid(np.arange(n_r), np.linspace(0, 2*np.pi, n_theta))
        R, T = np.meshgrid(radcoords[:, i]*15.6, np.linspace(0, 2*np.pi, n_theta))
        c = ax.pcolormesh(T, R, hslice.T, vmin=vmin, vmax=vmax, cmap='RdBu_r', rasterized=True)
        #ax.set_rmax(64*15.6)
        cbar = plt.colorbar(c, ax=ax, shrink=0.9, pad=0.1)
        cbar.ax.set_ylabel('Radial Displacement [m]')

        ax.axvline(l1*3.6*(np.pi/180), color='purple')
        ax.axvline((l1+50)*3.6*(np.pi/180), color='green')
#        ax.set_yticks([0, 100, 200])
        ax.set_rlabel_position(90)
        ax.set_theta_zero_location('N')
        ax.set_theta_direction(-1)
        ax.grid()
        label_position = ax.get_rlabel_position()
        ax.text(
            np.radians(label_position + 10),
            ax.get_rmax() / 2.,
            'Distance [km]',
            ha='center',
            va='center')
    
        # tick locations
        thetaticks = np.arange(0, 360, 45)
        # set ticklabels location at 1.3 times the axes' radius
        ax.set_thetagrids(thetaticks, frac=1.2)
    
        ax.set_xlabel("Angle around flux\n surface [degrees]")
    
#        plot = manager.save_figure('rdisplacement_cplot_{}'.format(cfg.get_identifier()).replace('.', '-'), fig=fig, fext='.png')
#        plot.caption = 'Height: {} km'.format(height)
    
#        dplots.append(plot)

        ax1 = fig.add_subplot(122)
        r = radcoords[::2, i]*15.6
        disline1 = hslice[::2, l1]
        disline2 = hslice[::2, l1+50]
    
        ax1.scatter(r, disline1, color='purple', marker='x', label=r'${}^\circ$'.format(l1*3.6))
        ax1.scatter(-r[::-1], disline2[::-1], color='green', marker='s', label=r'${}^\circ$'.format((l1+50)*3.6))
        ax1.axhline(0, linestyle='--', color='black')
        #ax.set_xlim(0, 270)
        vmin, vmax = -plotlims[i], plotlims[i]
        ax1.set_ylim(vmin, vmax)
        ax1.set_xlabel('Distance from axis [km]')
        ax1.set_ylabel('Displacement [m]')
        plt.legend(loc='upper left')

        fig.tight_layout(pad=2.0)
        #plt.savefig(os.path.join('paper', 'Figs', '{}_hslice_h{:03}_t{:03}'.format(cfg.get_identifier(), h, time)))
        plt.savefig(os.path.join('tempfigs', '{}_hslice_h{:03}_t{:03}'.format(cfg.get_identifier(), h, time)))
        plt.close()
