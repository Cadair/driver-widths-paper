import os
import numpy as np
from matplotlib import pyplot as plt
import plotting_helpers as ph
from sacconfig import SACConfig
import yt
import texfigure
 

os.system('./configure.py set driver --delta_x={0} --delta_y={0} --amp={1} --fort_amp={2}'.format(0.35, '4.1', '4.159470035d0'))
#os.system('./configure.py set driver --delta_x={0} --delta_y={0} --amp={1} --fort_amp={2}'.format(0.15, '10.', '10.221149567d0'))
cfg = SACConfig()

### Time slices with time-displacement plots
radii = ['r30']
#l1 = 38
l1 = 25
#time = 210

for time in range(10, 270, 1):
    ds = yt.load('/home/drew/fastdata/driver-widths/gdf/{0}/{0}_{1:05}.gdf'.format(cfg.get_identifier(), time))
    simtime = ds.current_time.to('s')
    
    ### Height slice(s) with distance-displacement plots
    radii = range(3, 66, 3)
    heights = range(15, 125, 50)
    
    alldists = np.zeros((len(radii)*2, len(heights), 270, 100))
    radcoords = np.zeros((len(radii)*2, len(heights)))
    
    for j, r in enumerate(radii):
        tube_r = 'r{:02}'.format(r)
        for i, h in enumerate(heights):
            dis = np.load(os.path.join(cfg.data_dir, tube_r, "{}_h{}_distance.npy".format(cfg.get_identifier(), h)))[:270]
            vphi = np.load(os.path.join(cfg.data_dir, tube_r, "{}_h{}_vphi.npy".format(cfg.get_identifier(), h)))[:270]
            #import pdb; pdb.set_trace()
            n_t, n_theta = dis.shape
            radcoords[j*2, i] = dis.mean()
            d1 = vphi#dis - dis[0][None]
            #d1 *= 15.6e3
    
            alldists[j*2, i, :, :] = d1
    
    sep = 1.04 #0.5
    radcoords[1::2] = radcoords[::2] * sep
    plotlims = [450, 320, 500, 650]
    radlims = [90, 450, 1000]
    
    dplots = texfigure.MultiFigure(3, 2)
    dplots.caption = r"""Radial displacement of points located on several flux surfaces throughout the domain at time $t = {}$ s.
        Note that the radial extent on each of these plots is different due to the expansion of the flux tube at greater heights in the domain.
        Left column: polar plots showing motion of the points away from (red) or towards (blue) the flux tube axis.
        The radial and azimuthal axes indicate the location of each point with respect to the axis.
        Right column: displacement of points located at ${}^\circ$ (purple crosses) and  ${}^\circ$ (green circles), indicated on the polar plots by radial lines of the same colour.
        We can see see in these plots evidence of the kink mode low in the domain and the sausage mode near the top.
        In between we see both waves, with the kink confined close to the flux tube axis and the sausage seen closer to the edges.
    """.format(str(simtime)[:5], l1*3.6, (l1+50)*3.6)
    dplots.label = 'fig:hslices'
    
    #figsize = texfigure.figsize(pytex, figure_width_context='textwidth')
    for i, h in enumerate(heights):
        height = h * 12.5
        fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})#, figsize=figsize)
        hslice = alldists[:, i, time, :]
        n_r, n_theta = hslice.shape
        sym_max = np.max((np.abs(np.min(hslice)), np.max(hslice)))
        vmin = sym_max * -1.
        vmax = sym_max
    
        R, T = np.meshgrid(radcoords[:, i]*15.6, np.linspace(0, 2*np.pi, n_theta))
        #c = ax.pcolormesh(T, R, hslice.T, cmap='RdBu_r', rasterized=True, vmin=vmin, vmax=vmax)
        c = ax.pcolormesh(T, R, hslice.T, cmap='PRGn', rasterized=True, vmin=vmin, vmax=vmax)
        maxrad = radlims[i]
        ticks = [(tik//10)*10 for tik in np.linspace(0, maxrad, 5)]
        ax.set_rmax(maxrad)
        cbar = plt.colorbar(c, ax=ax, shrink=0.9, pad=0.1)
        cbar.ax.set_ylabel('Azimuthal velocity [m/s?]')
    
        ax.axvline(l1*3.6*(np.pi/180), color='purple')
        ax.axvline((l1+50)*3.6*(np.pi/180), color='green')
        ax.set_yticks(ticks)
        ax.set_yticklabels([])
        ax.set_theta_zero_location('N')
        ax.set_theta_direction(-1)
        ax.grid()
    
        # tick locations
        thetaticks = np.arange(0, 360, 45)
        # set ticklabels location at 1.3 times the axes' radius
        ax.set_thetagrids(thetaticks, frac=1.2)
    
        ax.set_xlabel("Angle around flux\n surface [degrees]")
    
        box=ax.get_position()
        axl=fig.add_axes([box.xmin/1.5, #put it about half way between the edge of the 1st subplot and the left edge of the figure
                          0.5*(box.ymin+box.ymax), #put the origin at the same height of the origin of the polar plots
                          box.width/40, #Doesn't really matter, we will set everything invisible, except the y axis
                          box.height*0.5], #fig.subplots_adjust will not adjust this axis, so we will need to manually set the height to 0.4 (half of 0.9-0.1)
                          axisbg=None) #transparent background.
        axl.spines['top'].set_visible(False)
        axl.spines['right'].set_visible(False)
        axl.spines['bottom'].set_visible(False)
        axl.yaxis.set_ticks_position('both')
        axl.xaxis.set_ticks_position('none')
        axl.set_xticklabels([])
        axl.set_ylim(0, maxrad)
        axl.set_yticks(ticks)
        axl.set_ylabel('Distance [km]', rotation=90)
    
        fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9, wspace=0.5)
    
        #plot = manager.save_figure('rdisplacement_hslice_{}_h{:03}_t{:03}'.format(cfg.get_identifier(), h, time).replace('.', '-'), fig=fig, fext='.pdf')
        #plot.caption = 'Height: {} km'.format(height)
        #plot.subfig_width = r'\columnwidth'
    
        #dplots.append(plot)
        plt.savefig('tempfigs/vphicirc_{}_{}'.format(h, time))
    
        fig, ax = plt.subplots()#figsize=figsize)
        r = radcoords[::2, i]*15.6
        disline1 = hslice[::2, l1]
        disline2 = hslice[::2, l1+50]
    
        ax.scatter(r, disline1, color='purple', marker='x', label=r'${}^\circ$'.format(l1*3.6))
        ax.scatter(r, disline2, color='green', marker='o', label=r'${}^\circ$'.format((l1+50)*3.6))
        ax.axhline(0, linestyle='--', color='black')
        ax.set_ylim(vmin*1.2, vmax*1.2)
        ax.set_xlim(0, maxrad)
        ax.set_xlabel('Distance from axis [km]')
        ax.set_ylabel('Azimuthal velocity [m/s?]')
        #ax.set_xticklabels([str(int(abs(x))) for x in ax.get_xticks()])
        plt.legend(loc='best')
        fig.tight_layout()#pad=2.0)
    
        #plot = manager.save_figure('rdisplacement_hline_{}_h{:03}_t{:03}'.format(cfg.get_identifier(), h, time).replace('.', '-'), fig=fig, fext='.pdf')
        #plot.caption = 'Height: {} km'.format(height)
        #plot.subfig_width = r'\columnwidth'
    
        #dplots.append(plot)
        plt.savefig('tempfigs/vphiline_{}_{}'.format(h, time))
        
    #print(dplots._repr_latex_())
    plt.close('all')
    
