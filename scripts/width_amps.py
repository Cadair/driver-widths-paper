from collections import namedtuple

__all__ = ['sim_params']

WidthAmp= namedtuple('WidthAmp', ['delta_x', 'amp', 'fort_amp'])

widths = ('0.15', '0.20', '0.25', '0.30', '0.35')
#amps=('7.2274441701395986', '5.2788458200228927', '4.167609775756989', '3.4471974045012397', '2.9411894675997523')
amps=['10.221149567', '7.465415352', '5.893890268', '4.875073322', '4.159470035']

str_amps = amps
amps = [float(a[:3]) for a in str_amps]

# These are the same as the parameters in cfg.
sim_params = tuple([WidthAmp(delta_x=w, amp=a, fort_amp=f+'d0') for w, a, f in zip(widths, amps, str_amps)])

